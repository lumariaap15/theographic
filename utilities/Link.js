import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

export default class Link extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        return (
            <View>
                <TouchableOpacity><Text style={styles.link}>{this.props.text}</Text></TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    link:{
        color: 'blue',
    }
})