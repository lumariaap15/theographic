import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import TextRegular from '../utilities/TextRegular';
import TextBold from '../utilities/TextBold';
import { Card } from 'native-base';

export default class PlacesComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {

        return (
            <View>
                <Text style={{ marginBottom: 5 }}>Places</Text>
                {this.props.places.map(place => {
                    // startEndArray = verse.verseText.split(this.props.query);
                    return (
                        <Card style={styles.placeCard} key={"container" + place.slug}>
                            <View>
                                <Text style={styles.name}>{place.name}</Text>
                                <Text>
                                    <TextRegular style={{ display: 'inline' }} text={place.verseCount + " verses. First mentioned in " + place.verses[0].fullRef} />
                                    {/*
                                        <TextBold style={{ display: 'inline' }} text={" " + this.props.query + " "}></TextBold>
                                        <TextRegular style={{ display: 'inline' }} text={startEndArray[1]}></TextRegular>
                                    */}
                                </Text>
                            </View>
                        </Card>
                    )
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    placeCard: {
        marginBottom: 15,
        borderRadius: 10,
        padding: 15
    },
    name: {
        color: 'blue',
        fontSize: 18,
        fontFamily: 'Roboto'
    },
})


