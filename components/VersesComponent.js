import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import TextRegular from '../utilities/TextRegular';
import TextBold from '../utilities/TextBold';
import { Card } from 'native-base';

export default class VersesComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {

        return (
            <View>
                <Text style={{ marginBottom: 5 }}>Verses</Text>
                {this.props.verses.map(verse => {
                    // startEndArray = verse.verseText.split(this.props.query);
                    return (
                        <Card style={styles.verseCard} key={"container" + verse.verseId}>
                            <View>
                                <Text style={styles.verseReference}>{verse.fullRef}</Text>
                                <Text>
                                    <TextRegular style={{ display: 'inline' }} text={verse.verseText} />
                                    {/*
                                        <TextBold style={{ display: 'inline' }} text={" " + this.props.query + " "}></TextBold>
                                        <TextRegular style={{ display: 'inline' }} text={startEndArray[1]}></TextRegular>
                                    */}
                                </Text>
                            </View>
                        </Card>
                    )
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    verseCard: {
        marginBottom: 15,
        borderRadius: 10,
        padding: 15
    },
    verseReference: {
        color: 'blue',
        fontSize: 18,
        fontFamily: 'Roboto'
    },
})


