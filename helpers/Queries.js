import gql from 'graphql-tag';

const versesQuery = gql`
query verses($input:String!) {
  searchVerses(input:$input){
    verseText  
    verseId
    shortRef
    fullRef
  }
}
`;

const peopleQuery = gql`
query($input:String!) {
  searchPeople(input:$input){
    gender
    name
    description
    personId
  }
}
`;

const placesQuery = gql`
query($input:String!) {
  searchPlaces(input:$input){
    placeId
    name
    description
  }
}
`;

export default {placesQuery, peopleQuery, versesQuery};